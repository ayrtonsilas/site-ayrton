$(function(){
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
});

$("#contact_form").submit(function(){
    event.preventDefault();
    var dados = $(this).serialize();
    console.log(dados);
    toastr.info('Ainda não está sendo possível enviar mensagem, por favor entre em contato por telefone ou e-mail!');
    return;
    $.ajax({
        type: "POST",
        url: "http://www.ayrtonsilas.com.br/process.php",
        data: dados, 
        success: function(data)
        {
            alert(data); 
        }
      });
});